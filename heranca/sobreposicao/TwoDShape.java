// Usa o despacho dinâmico de métodos
class TwoDShape {
    private double width;
    private double height;
    private String name;

    // Construtor padrão
    TwoDShape() {
        width = height = 0.0;
        name = "none";
    }

    // Construtor parametrizado
    TwoDShape(double w, double h, String n) {
        width = w;
        height = h;
        name = n;
    }

    // Constrói objeto com largura e altura iguais
    TwoDShape(double x, String n) {
        width = height = x;
        name = n;
    }

    // Contrói um objeto a partir de outro
    TwoDShape(TwoDShape ob) {
        width = ob.width;
        height = ob.height;
        name = ob.name;
    }

    // Métodos acessadores para width e heigh
    double getWidth() {return width; }
    void setWidth(double w) { width = w; }
    double getheight() {return height; }
    void setheight(double h) {height = h; }
    String getName() {return name; }

    void showDim() {
        System.out.println("Width is " + width + " and height is " + height);
    }

    double area() {
        System.out.println("area() must be overridden");
        return 0.0;
    }
}

// Subclasse de TwoDShape para triângulos
class Triangle extends TwoDShape {
    private String style;

    // Construtor padrão
    Triangle() {
        super();
    }

    // Contrutor para Triangle
    Triangle(String s, double w, double h) {
        super(w, h, "Triangule"); 
        style = s;
    }

    // Construtor com um argumento
    Triangle(double x) {
        super(x, "Triangle"); // chama construtor da superclass
        style = "filled";
    }

    // Contrói um objeto a partir de outro
    Triangle(Triangle ob) {
        super(ob); // passa o objeto para o construtor de TwoDShape
        style = ob.style;
    }

    // Sobrepõe area() para Triangle
    double area() {
        return getWidth() * getheight() / 2;
    }

    void showStyle() {
        System.out.println("Triangle style is " + style);
    }
}

// Subclasse de TwoDShape para triângulos
class Rectangle extends TwoDShape {
    // Construtor padrão
    Rectangle() {
        super();
    }

    // Contrutor para Rectangle
    Rectangle(double w, double h) {
        super(w, h, "Rectangle");
    }

    // Construtor com um argumento
    Rectangle(double x) {
        super(x, "Rectangle"); // chama construtor da superclass
    }

    // Contrói um objeto a partir de outro
    Rectangle(Rectangle ob) {
        super(ob); // passa o objeto para o construtor de TwoDShape
    }

    // Sobrepõe area() para Rectangle
    double area() {
        return getWidth() * getheight() / 2;
    }

    boolean isSquare() {
        if (getWidth() == getheight()) return true;
        return false;
    }
}

class DynShapes {

    public static void main (String args[]) {
        TwoDShape shapes[] = new TwoDShape[5]; // Array de TwoDShape (Superclass)

        shapes[0] = new Triangle("outlined", 8.0, 2.0);
        shapes[1] = new Rectangle(10);
        shapes[2] = new Rectangle(10.0, 4);
        shapes[3] = new Triangle(7.0);
        shapes[4] = new TwoDShape(10, 20, "generic");

        for (int i = 0; i < shapes.length; i++) {
            System.out.println("Object is " + shapes[i].getName());
            System.out.println("Area is " + shapes[i].area());
            // if (i == 3) shapes[i].showStyle(); // não compila, pois o objeto é uma superclass e o método showStyle() é da subclass Triangle
            // isso é sobreposição de métodos
            System.out.println();
        }

        
    }
}