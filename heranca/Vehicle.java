class Vehicle {
    private int passengers; // número de passageiros
    private int fuelcap; // capacidade de armazenamento
                        // de combustível em galões
    private int mpg; // consumo de combustível em milhas por galões

    // Este é um construtor para Vehicle
    Vehicle(int p, int f, int m) {
        passengers = p;
        fuelcap = f;
        mpg = m;
    }

    // Retorna a autonomia
    int range() {
        return mpg * fuelcap;
    }

    // Calcula o combustível necessário para uma dada distância
    double fuelNeeded(int miles) {
        return (double) miles / mpg;
    }

    // Métodos de acesso de variáveis de instância
    int getPassangers() { return passengers; }
    void setPassangers(int p) { passengers = p; }
    int getFuelCap() { return fuelcap; }
    void setFuelcap(int f) { fuelcap = f; }
    int getMpg() { return mpg; }
    void setMpg(int m) {mpg = m; }

}

// Estende Vehicle para criar a especialização Truck
class Truck extends Vehicle {
    private int cargocap;

    // Este é um construtor para Truck
    Truck(int p, int f, int m, int c) {
        /* Inicializa membros de Vehicle usando o construtor de Vehicle*/
        super(p, f, m);

        cargocap = c;
    }

    // Métodos acessadores para cargocap
    int getCargoCap() { return cargocap; }
    void setCargoCap(int c) { cargocap = c; }

}

class TruckDemo {
    public static void main(String [] args) {

        // constrói alguns caminhões
        Truck semi = new Truck(2, 200, 7, 44000);
        Truck pickup = new Truck(3, 28, 15, 2000);
        double gallons;
        int dist = 252;

        gallons = semi.fuelNeeded(dist);

        showData(semi, "semi", dist, gallons);
        showData(pickup, "pickup", dist, gallons);
    }

    static void showData(Truck t, String vehicle, int dist, double gallons) {

        System.out.println("Semi can carry " + t.getCargoCap() + " pounds.");

        System.out.println("To go " + dist + " miles semi needs " + gallons + " gallons of fuel.");

        System.out.println();

    }
}