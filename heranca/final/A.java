class A {
    final void meth() {
        System.out.println("This is a final method");
    }
}

class B extends A {
    // void meth() { // Erro! Não pode sobrepor
    //    System.out.println("Illegal");
    //}
}