class A {
    A() {
        System.out.println("Constructin A.");
    }
    int i;
}

class B extends A {
    B() {
        System.out.println("Constructin B.");
    }
    int i; // essa variável i oculta a variável i de A.

    B(int a, int b) {
        super.i = a; // i de A
        i = b; // i de B
    }

    void show() {
        System.out.println("i in superclass: " + super.i);
        System.out.println("i in subclass: " + i);
    }
}

class C extends B {
    C() {
        System.out.println("Constructin C.");
    }
}

class UseSuper {
    public static void main (String []args) {
        /* B subOb = new B(1, 2);
        subOb.show(); */
        C c = new C();
    }
}