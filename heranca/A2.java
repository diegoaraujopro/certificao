class A2 {
    int i , j;

    A2() {
        System.out.println("Constructin A1.");
    }

    A2(int a, int b) {
        i = a;
        j = b;
    }

    // exibe i e j
    void show() {
        System.out.println("i and j: " + i + " " + j );
    }
}

class B2 extends A2 {
    int i; // essa variável i oculta a variável i de A.
    int k;

    B2() {
        System.out.println("Constructin B1.");
    }

    B2(int a, int b, int c) {
        super (a, b);
        k = c;
    }

    // sobrecarrega show()
    void show(String msg) {
        System.out.println(msg + k);
    }
}

class Overload {
    public static void main (String []args) {
        B2 subOb = new B2(1, 2, 3);
        subOb.show("This is k:"); // chama show em B2
        subOb.show(); // chama show() em A2  
    }
}