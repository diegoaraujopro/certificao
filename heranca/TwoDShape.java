// hierarquia de classe simples

// Uma classe para objetos de duas dimensões
class TwoDShape {
    private double width; // agora esses 
    private double height; // membros são privados

    // Construtor padrão
    TwoDShape() {
        width = height = 0.0;
    }

    TwoDShape(double x) {
        width = height = x;
    }

    // Construtor parametrizado
    TwoDShape(double w, double h) {
        width = w;
        height = h;
    }

    // Métodos acessadores para width e height
    double getWidth() { return width; }
    double getHeight() { return height; }
    void setWidth(double w) { width = w; }
    void setHeight(double h) { height = h; }

    void showDim() {
        System.out.println("Width and Height are " + width + " and " + height);
    }
}

// Uma subclasse de TwoDShape para triângulos
class Triangle extends TwoDShape {
    private String style;

    Triangle() {
        super();
        style = "none";
    }

    Triangle(double x) {
        super(x); // chama construtor da superclasse
        style = "filled";
    }

    Triangle(String s, double w, double h) {
        super(w, h); // chama construtor da superclasse
        style = s;
    }
    double area() {
        return getWidth() * getHeight() / 2;
    }

    void showStyle() {
        System.out.println("Triangle is " + style);
    }

}

class ColorTriangle extends Triangle {
    private String color;

    ColorTriangle(String c, String s, double w, double h) {
        super(s, w, h);
        color = c;
    }

    String getColor() { return color; }

    void showColor() {
        System.out.println("Color is " + color);
    }
}

class Shapes {
    public static void main(String[] args) {
        Triangle t1 = new Triangle();
        Triangle t2 = new Triangle("outlined", 8.0, 12.0);
        Triangle t3 = new Triangle("filled", 4.0, 4.0);

        showData(t1, "t1");
        showData(t2, "t2");
        showData(t3, "t3");
    }

    static void showData(Triangle t, String tt) {
        System.out.println("Info for " + tt + ": ");
        t.showStyle();
        t.showDim();
        System.out.println("Area is " + t.area());
        System.out.println();
    }
}


class Shapes6 {
    public static void main(String[] args) {
        ColorTriangle t1 = new ColorTriangle("blue", "outlined", 8.0, 12.0);
        ColorTriangle t2 = new ColorTriangle("red", "filled", 2.0, 2.0);

        showData(t1, "t1");
        showData(t2, "t2");
    }

    static void showData(ColorTriangle t, String tt) {
        System.out.println("Info for " + tt + ": ");
        t.showStyle();
        t.showDim();
        t.showColor();
        System.out.println("Area is " + t.area());
        System.out.println();
    }
}