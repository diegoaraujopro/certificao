class A1 {
    int i , j;

    A1() {
        System.out.println("Constructin A1.");
    }

    A1(int a, int b) {
        i = a;
        j = b;
    }

    // exibe i e j
    void show() {
        System.out.println("i and j: " + i + " " + j );
    }
}

class B1 extends A1 {
    int i; // essa variável i oculta a variável i de A.
    int k;

    B1() {
        System.out.println("Constructin B1.");
    }

    B1(int a, int b) {
        super.i = a; // i de A
        i = b; // i de B
    }

    B1(int a, int b, int c) {
        super (a, b);
        k = c;
    }

    // exibe k - essa versão sobrepõe show() em A
    void show() {
        super.show();
        System.out.println("k: " + k);
    }
}

class Override {
    public static void main (String []args) {
        B1 subOb = new B1(1, 2, 3);
        subOb.show(); // chama show() em B
        
    }
}