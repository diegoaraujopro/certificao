import java.util.Arrays;

class Arrays2 {

    public static void main (String args[]) {
        int a[] = {1, 2, 3, 4, 5, 6};
        int []b = new int[5];

        char c [] = new char[3];
        c[1]='a';
        c[2]='b';

        char d [] = {'e','e',' '};
        
        System.arraycopy(a, 0, b, 3, 2);

        for (int i = 0; i < b.length; i++)
            System.out.println(b[i]);

        System.arraycopy(c, 1, d, 0, 2);

        for (int i = 0; i < d.length; i++)
            System.out.printf("d[%d] = '%c' %n",i, d[i]);

        
    }
}