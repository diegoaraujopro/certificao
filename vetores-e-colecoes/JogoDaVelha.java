class JogoDaVelha {
    public static void main (String [] args) {
        char [] jogo[] = new char [3] [3];
        jogo[0][0] = 'x';
        jogo[0][1] = 'o';
        jogo[0][2] = 'o';

        jogo[1][0] = 'x';
        jogo[1][1] = 'x';
        jogo[1][2] = 'o';

        jogo[2][0] = 'o';
        jogo[2][1] = 'x';
        jogo[2][2] = 'o';

        for (int l = 0; l < jogo.length; l++ ) {
            for (int c = 0; c < jogo[l].length; c++) {
                if (c != 2)
                    System.out.print(jogo[l][c]+"|");
                else
                    System.out.println(jogo[l][c]);
            }
        }
    }
}