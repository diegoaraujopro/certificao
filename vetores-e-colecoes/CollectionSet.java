import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

class CollectionSet {

    public static void main (String args[]) {
        Set<String> nomesAmigos = new HashSet<String>();

        String nome = "";
        

        while (!nome.equals("sair")) {
            System.out.print("Informe o nome de seus amigos: ");
            Scanner entrada = new Scanner(System.in);
            nome = entrada.nextLine();

            if (!nome.equals("sair")) 
                nomesAmigos.add(nome);
        }

        System.out.println("Seus amigos são: ");

        Iterator<String> it = nomesAmigos.iterator();

        while(it.hasNext()) 
            System.out.println(it.next());

    }

}