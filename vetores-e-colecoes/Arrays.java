class Arrays {

    public static void main (String [] args) {
        char [] vetor = new char[5];
        vetor[0] = 'A';

        System.out.printf("Meu array 'vetor[]' tem tamanho %d. E o índice 0 é %c.%n", vetor.length, vetor[0]);

        char etapa[] = {'A', 'B', 'C', 'D', 'E'};

        System.out.printf("Meu array '[]etapa' tem tamanho %d. E o índice 3 é %c.", etapa.length, etapa[2]);
        System.out.printf("%n é uma quebra de linha. %n %n");
    }
}