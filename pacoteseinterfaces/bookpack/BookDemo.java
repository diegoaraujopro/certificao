package bookpack;

class Book {

    private String title;
    private String author;
    private int pubDate;

    Book(String t, String a, int d) {
        title = t;
        author = a;
        pubDate = d;
    }

    void show() {
        System.out.println(title);
        System.out.println(author);
        System.out.println(pubDate);
        System.out.println();
    }
}

class Author extends Book { // Pode herdar dentro do mesmo pacote (Defautl)
	
}


class BookDemo {

    public static void main (String []args) {
        Book books[] = new Book[5];

        books[0] = new Book("Java 1", "Author 1", 2014);
        books[1] = new Book("Java 2", "Author 2", 2016);
        books[2] = new Book("Java 3", "Author 3", 2017);
        books[3] = new Book("Java 4", "Author 4", 2018);
        books[4] = new Book("Java 5", "Author 5", 2019);

        for (int i = 0; i < books.length; i ++) books[i].show();
    }
}